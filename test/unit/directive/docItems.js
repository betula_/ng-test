'use strict';

describe('docItems directive', function(){
  var elm, scope, items;

  beforeEach(function() {
    module('App');

    items = [
      { "_id": { "$oid": "51e5db66e4b0ba1f8763c34f"}, "title": "First document", "description": "description"} ,
      { "_id": { "$oid": "51e5de50e4b054a5b8a1ad17"}, "title": "Second document", "description": "Short description..."}
    ];

    inject(function($rootScope, $compile) {
      elm = angular.element('<doc-items items="items"></doc-items>');
      scope = $rootScope;
      scope.items = items;
      $compile(elm)(scope);
      scope.$digest();
    });
  });

  it('should have list of items', function() {
    expect(elm.find('> ul > li[item="item"]').length).toBe(items.length);
    items.push({
      _id: items[0]._id,
      title: items[0].title,
      description: items[0].description
    });
    scope.$digest();
    expect(elm.find('> ul > li[item="item"]').length).toBe(items.length);
    expect(items.length).toBe(3);
  });


});
