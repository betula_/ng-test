'use strict';

describe('docView directive', function(){
  var elm, scope, rootScope, doc, enterKeyDown, setInputValue;

  beforeEach(function() {
    module('App');

    inject(function($q) {
      doc = {
        title: 'Test title',
        description: 'Test description'
      };
      doc.$promise = $q.when(doc);
    });

    inject(function($rootScope, $compile) {
      elm = angular.element('<doc-view doc="doc"></doc-view>');
      rootScope = $rootScope;
      rootScope.doc = doc;
      $compile(elm)(rootScope);
      rootScope.$digest();
      scope = elm.scope();
      scope.$doc = {
        save: jasmine.createSpy('$doc.save')
      };
    });

    enterKeyDown = jQuery.Event('keydown', { keyCode: 13 });

    inject(function($sniffer) {
      setInputValue = function(el, val) {
        el.val(val);
        el.trigger($sniffer.hasEvent('input') ? 'input' : 'change');
      };
    });

  });


  it('should be isolated scope', function() {
    rootScope.value = 10;
    rootScope.$digest();
    expect(scope.value).not.toBeDefined();
  });

  it('should pass doc attribute', function() {
    expect(scope.doc).toEqual(rootScope.doc);
  });

  it('should have form, title and description controls', function() {
    expect(elm.find('form').length).toBe(1);
    expect(elm.find('[ng-model="$doc.local.title"]').length).toBe(1);
    expect(elm.find('[ng-model="$doc.local.description"]').length).toBe(1);
    expect(elm.find('[ng-click="$doc.save()"]').length).toBeGreaterThan(0);
  });

  it('should show/hide edit panel for title', inject(function($parse) {
    var view = elm.find('dd > [ng-hide="$doc.local.$editable.title"]');
    var edit = elm.find('dd > [ng-show="$doc.local.$editable.title"]');
    expect(view.length).toBe(1);
    expect(edit.length).toBe(1);
    var btnEdit = view.find('[ng-click^="$doc.local.$editable.title"][ng-click$="true"]');
    var btnCancel = edit.find('[ng-click^="$doc.local.$editable.title"][ng-click$="false"]');
    expect(btnEdit.length).toBe(1);
    expect(btnCancel.length).toBe(1);
    expect($parse('$doc.local.$editable.title')(scope)).toBeFalsy();
    btnEdit.click();
    scope.$digest();
    expect($parse('$doc.local.$editable.title')(scope)).toBeTruthy();
    btnCancel.click();
    scope.$digest();
    expect($parse('$doc.local.$editable.title')(scope)).toBeFalsy();
  }));

  it('should show/hide edit panel for description', inject(function($parse) {
    var view = elm.find('dd > [ng-hide="$doc.local.$editable.description"]');
    var edit = elm.find('dd > [ng-show="$doc.local.$editable.description"]');
    expect(view.length).toBe(1);
    expect(edit.length).toBe(1);
    var btnEdit = view.find('[ng-click^="$doc.local.$editable.description"][ng-click$="true"]');
    var btnCancel = edit.find('[ng-click^="$doc.local.$editable.description"][ng-click$="false"]');
    expect(btnEdit.length).toBe(1);
    expect(btnCancel.length).toBe(1);
    expect($parse('$doc.local.$editable.description')(scope)).toBeFalsy();
    btnEdit.click();
    scope.$digest();
    expect($parse('$doc.local.$editable.description')(scope)).toBeTruthy();
    btnCancel.click();
    scope.$digest();
    expect($parse('$doc.local.$editable.description')(scope)).toBeFalsy();
  }));

  it('should title required', inject(function($parse, $browser) {
    var title = elm.find('[ng-model="$doc.local.title"]');
    setInputValue(title, '');
    expect(scope.form.$invalid).toBe(true);
    expect(title.length).toBe(1);
    expect(title.is('[required]')).toBe(true);
    setInputValue(title, 'Title');
    expect($parse('$doc.local.title')(scope)).toBe('Title');
    expect(scope.form.$invalid).toBe(false);
  }));

  it('should save throw title edit panel', function() {
    var title = elm.find('[ng-model="$doc.local.title"]');
    setInputValue(title, '');
    title.trigger(enterKeyDown);
    expect(scope.$doc.save).not.toHaveBeenCalled();
    var btn = elm.find('[ng-show="$doc.local.$editable.title"] [ng-click="$doc.save()"]');
    expect(btn.length).toBe(1);
    expect(btn.is(':disabled')).toBe(true);
    setInputValue(title, 'Title!');
    expect(btn.is(':disabled')).toBe(false);
    title.trigger(enterKeyDown);
    expect(scope.$doc.save).toHaveBeenCalled();
    btn.click();
    expect(scope.$doc.save.calls.length).toBe(2);
  });

  it('should save throw description edit panel', function() {
    var description = elm.find('[ng-model="$doc.local.description"]');
    var btn = elm.find('[ng-show="$doc.local.$editable.description"] [ng-click="$doc.save()"]');
    btn.click();
    expect(scope.$doc.save).toHaveBeenCalled();
  });


});
