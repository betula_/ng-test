'use strict';

describe('header directive', function() {
  var elm, scope, page;

  var mock = {};
  mock.page = function() {
    this.title = '';
  };

  beforeEach(function() {
    module('App');

    page = new mock.page();
    module(function($provide) {
      $provide.value('page', page);
    });

    inject(function($rootScope, $compile) {
      elm = angular.element('<header></header>');
      scope = $rootScope;
      $compile(elm)(scope);
      scope.$digest();
    });

  });

  it('should have a page title', function() {
    var el;
    el = elm.find('a.brand');
    expect(el.text()).toBe(page.title);
  });

  it('should binging to a page service', function() {
    var el;
    el = elm.find('a.brand');
    expect(el.text()).toBe(page.title);
    page.title = 'Test title!';
    scope.$digest();
    expect(el.text()).toBe('Test title!');
  });


});
