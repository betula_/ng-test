'use strict';

describe('enterKeyApply directive', function(){
  var scope, eventEnter, eventOther;

  beforeEach(function() {
    module('App');
    inject(function($rootScope) {
      scope = $rootScope;
      scope.do = jasmine.createSpy();
    });
    eventEnter = jQuery.Event('keydown', { keyCode: 13 });
    eventOther = jQuery.Event('keydown', { keyCode: 14 });
  });

  describe('without parent form', function() {
    var elm;

    beforeEach(function() {
      inject(function($compile) {
        elm = angular.element('<div enter-key-apply="do()"></div>');
        $compile(elm)(scope);
        scope.$digest();
      });
    });

    it('should call do function after enter key press', function() {
      expect(scope.do).not.toHaveBeenCalled();
      elm.trigger(eventOther);
      expect(scope.do).not.toHaveBeenCalled();
      elm.trigger(eventEnter);
      expect(scope.do).toHaveBeenCalled();
    });

    it('should be use angular parse style expression', inject(function($compile) {
      var elm = angular.element('<div enter-key-apply="b.run(value.a, value)"></div>');
      $compile(elm)(scope);
      scope.$digest();
      scope.b = { run: jasmine.createSpy() };
      scope.value = { a:0, b:'b', c:null };
      elm.trigger(eventEnter);
      expect(scope.b.run).toHaveBeenCalledWith(0, { a:0, b:'b', c:null });
    }));

  });

  describe('with parent form', function() {
    var form, elm;

    beforeEach(function() {
      inject(function($compile) {
        form = angular.element('<form name="form"><div enter-key-apply="do()"></div></form>');
        $compile(form)(scope);
        scope.$digest();
        elm = form.children();
      });
    });

    it('should call do function if form is valid', function() {
      expect(scope.form.$valid).toBe(true);
      expect(scope.do).not.toHaveBeenCalled();
      form.trigger(eventEnter);
      expect(scope.do).not.toHaveBeenCalled();
      elm.trigger(eventOther);
      expect(scope.do).not.toHaveBeenCalled();
      elm.trigger(eventEnter);
      expect(scope.do).toHaveBeenCalled();
    });

    it('should not call do function if form is invalid', function() {
      expect(scope.form.$valid).toBe(true);
      scope.form.$setValidity(false);
      expect(scope.form.$valid).toBe(false);
      expect(scope.do).not.toHaveBeenCalled();
      elm.trigger(eventEnter);
      expect(scope.do).not.toHaveBeenCalled();
    });


  });



});
