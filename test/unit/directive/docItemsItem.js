'use strict';

describe('docItemsItem directive', function(){
  var elm, scope, item;

  beforeEach(function() {
    module('App');

    item = { "id": "51e5db66e4b0ba1f8763c34f", "title": "First document", "description": "description"};

    inject(function($rootScope, $compile) {
      elm = angular.element('<doc-items-item item="item"></doc-items-item>');
      scope = $rootScope;
      scope.item = item;
      $compile(elm)(scope);
      scope.$digest();
    });
  });

  it('should have title and description', function() {
    expect(elm.find('h4').text()).toMatch(item.title);
    expect(elm.text()).toMatch(item.description);
  });

  it('should be bind to item object', function() {
    var title = 'Title other';
    var description = 'Description other';
    scope.item.title = title;
    scope.item.description = description;

    expect(elm.find('h4').text()).not.toMatch(title);
    expect(elm.text()).not.toMatch(description);
    scope.$digest();
    expect(elm.find('h4').text()).toMatch(title);
    expect(elm.text()).toMatch(description);
  });

  it('should have link to full page', function() {
    expect(elm.find('a[ng-href="#!/'+item.id+'"]').length).toBe(1);
  });

  it('should have bind link to full page', function() {
    var id = 'other id';
    scope.item.id = id;
    expect(elm.find('a[ng-href="#!/'+id+'"]').length).toBe(0);
    scope.$digest();
    expect(elm.find('a[ng-href="#!/'+id+'"]').length).toBe(1);
  });

  it('should have remove button', function() {
    expect(elm.find('[ng-click="item.$remove()"]').length).toBe(1);
  });


});
