'use strict';

describe('docAddForm directive', function(){
  var elm, scope, rootScope;

  beforeEach(function() {
    module('App');
    inject(function($rootScope, $compile) {
      elm = angular.element('<doc-add-form apply="do(title, description)"></doc-add-form>');
      rootScope = $rootScope;
      rootScope.do = jasmine.createSpy();
      $compile(elm)(rootScope);
      rootScope.$digest();
      scope = elm.scope();
    });
  });


  it('should be isolated scope', function() {
    rootScope.value = 10;
    rootScope.$digest();
    expect(scope.value).not.toBeDefined();
  });

  it('should use angular expression in apply', function() {
    expect(scope.apply).toEqual(jasmine.any(Function));
    expect(scope.apply).not.toEqual(rootScope.do);
    scope.apply();
    expect(rootScope.do).toHaveBeenCalled();
    scope.apply({title: 'Test title!'});
    expect(rootScope.do).toHaveBeenCalledWith('Test title!', undefined);
  });

  it('should have title, description, and apply controls', function() {
    expect(elm.find('input[ng-model="doc.title"][required]').length).toBe(1);
    expect(elm.find('textarea[ng-model="doc.description"]').length).toBe(1);
    expect(elm.find('[ng-click="apply(doc)"]').length).toBeGreaterThan(0);
  });

  it('should title required', function() {
    expect(scope.form.$valid).toBe(false);
    scope.doc = {
      title: 'Test title'
    };
    rootScope.$digest();
    expect(scope.form.$valid).toBe(true);
  });

  it('should apply on button click', function() {
    scope.doc = {
      title: 'Test title',
      description: 'Test description'
    };
    rootScope.$digest();
    expect(scope.form.$valid).toBe(true);
    expect(rootScope.do).not.toHaveBeenCalled();
    elm.find('[ng-click^="apply"]').click();
    expect(rootScope.do).toHaveBeenCalledWith('Test title', 'Test description');
  });

  it('should apply then pressed enter key on title input', function() {
    var event = jQuery.Event('keydown', { keyCode: 13 });
    var title = elm.find('input[ng-model="doc.title"]');
    title.trigger(event);
    expect(rootScope.do).not.toHaveBeenCalled();
    scope.doc = {
      title: '1'
    };
    rootScope.$digest();
    title.trigger(event);
    expect(rootScope.do).toHaveBeenCalledWith('1', undefined);
  });

  it('should be disable button on invalid data', function() {
    var but = elm.find('[ng-click^="apply"]');
    expect(but.is(':disabled')).toBeTruthy();
    scope.doc = { title: 10 };
    scope.$digest();
    expect(but.is(':disabled')).not.toBeTruthy();
    scope.doc = { title: '' };
    scope.$digest();
    expect(but.is(':disabled')).toBeTruthy();
  });

});
