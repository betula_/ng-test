'use strict';

describe('footer directive', function(){
  var elm, scope;

  beforeEach(function() {
    module('App');
    inject(function($rootScope, $compile) {
      elm = angular.element('<footer></footer>');
      scope = $rootScope;
      $compile(elm)(scope);
      scope.$digest();
    });
  });

  it('should have copy info', function() {
    expect(elm.text()).toMatch('© Betula');
    expect(elm.find('a[href="http://betula.co"]').length).toBe(1);
    expect(elm.find('a[href="https://github.com/betula/ng-test"]').text()).toBe('View github repo');
  });


});
