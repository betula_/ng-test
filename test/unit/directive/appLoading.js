'use strict';

describe('appLoading directive', function() {
  var elm, scope;

  var mock = {};
  mock.appLoading = function() {
    this.status = 'ready';
  };

  var appLoading;

  beforeEach(function() {
    module('App');

    appLoading = new mock.appLoading();
    module(function($provide) {
      $provide.value('appLoading', appLoading);
    });

    inject(function($rootScope, $compile) {
      elm = angular.element('<app-loading></app-loading>');
      scope = $rootScope;
      $compile(elm)(scope);
      scope.$digest();
    });

  });

  it('should have not app-loading root element', function() {
    expect(elm.is('app-loading')).toBe(false);
  });

  it('should have element with loading status class', function() {
    expect(elm.find('.ready').length).toBe(1);
  });

  it('should change class of element when loading status changed', function() {
    var el;
    el = elm.find('.ready');
    appLoading.status = 'loading';
    scope.$digest();
    expect(el.is('.loading')).toBe(true);

    expect(elm.find('.ready').length).toBe(0);
    appLoading.status = 'ready';
    scope.$digest();
    expect(elm.find('.ready').length).toBe(1);
  });

});
