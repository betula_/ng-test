'use strict';

describe('PageDoc controller', function() {
  var ctrl, scope, doc;

  beforeEach(function() {
    module('App');
    doc = {};
    inject(function($rootScope, $controller) {
      scope = $rootScope.$new();
      ctrl = $controller('PageDoc', {
        $scope: scope,
        doc: doc
      });
    });
  });

  it('should encapsulate doc to scope', function() {
    expect(scope.doc).toEqual(doc);
  });



});