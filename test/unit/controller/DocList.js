'use strict';

describe('DocList controller', function() {
  var ctrl, scope, doc, items;

  beforeEach(function() {
    module('App');

    items = [
      { title: 'Title First' },
      { title: 'Title Second' }
    ];

    inject(function($q) {
      items[0].$remove = function() {
        return $q.when(items[0]);
      };
      spyOn(items[0], '$remove').andCallThrough();
      items[0].$$remove = items[0].$remove;
    });

    inject(function($q) {
      doc = {
        list: function() {
          var $items = items.slice();
          $items.$promise = $q.when($items);
          return $items;
        },
        insert: function(item) {
          return $q.when(item);
        }
      };
      spyOn(doc, 'list').andCallThrough();
      spyOn(doc, 'insert').andCallThrough();
    });

    inject(function($rootScope, $controller) {
      scope = $rootScope.$new();
      ctrl = $controller('DocList', {
        $scope: scope,
        doc: doc
      });
      spyOn(scope, 'addItem').andCallThrough();
      spyOn(scope, 'loadItems').andCallThrough();
    });

  });

  it('should have loadItems and addItem methods', function() {
    expect(scope.loadItems).toEqual(jasmine.any(Function));
    expect(scope.addItem).toEqual(jasmine.any(Function));
  });

  it('should load items on start', function() {
    expect(doc.list).toHaveBeenCalled();
    expect(scope.items.length).toBe(0);
    expect(scope.items.$promise).toBeUndefined();
    scope.$digest();
    expect(scope.items.length).toBe(2);
    expect(scope.items[0].title).toBe('Title First');
    expect(scope.items[1].title).toBe('Title Second');
  });

  it('should addItem call doc.insert', function() {
    scope.$digest();
    scope.addItem('Title Test!', 'Description Test!');
    expect(doc.insert).toHaveBeenCalledWith({ title: 'Title Test!', description: 'Description Test!'});
  });

  it('should reload items after addItem', function() {
    scope.$digest();
    scope.addItem();
    scope.$digest();
    expect(scope.loadItems.calls.length).toBe(1);
    expect(doc.list.calls.length).toBe(2);
    expect(doc.insert.calls.length).toBe(1);
  });

  it('should reload items after remove', function() {
    scope.$digest();
    scope.items[0].$remove();
    expect(items[0].$$remove).toHaveBeenCalled();
    expect(scope.loadItems).not.toHaveBeenCalled();
    scope.$digest();
    expect(scope.loadItems).toHaveBeenCalled();
  });


});