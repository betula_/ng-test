'use strict';

describe('Page controller', function() {
  var ctrl, scope, page;

  beforeEach(function() {
    module('App');
    page = {};
    inject(function($rootScope, $controller) {
      scope = $rootScope.$new();
      ctrl = $controller('Page', {
        $scope: scope,
        page: page
      });
    });
  });

  it('should encapsulate page to scope', function() {
    expect(scope.page).toEqual(page);
  });



});