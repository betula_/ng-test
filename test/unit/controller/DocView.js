'use strict';

describe('DocView controller', function() {
  var ctrl, scope, item, $document;

  beforeEach(function() {
    module('App');

    inject(function($q) {
      item = {};
      item.$promise = $q.when(item);
      item.$save = function() {
        return $q.when(item);
      };

      spyOn(item, '$save').andCallThrough();
    });

    inject(function($rootScope, $controller, _$document_, $parse) {
      $document = _$document_;
      spyOn($document, 'bind').andCallThrough();
      spyOn($document, 'unbind').andCallThrough();

      scope = $rootScope.$new();
      scope.doc = item;
      spyOn(scope, '$apply').andCallThrough();

      ctrl = $controller('DocView', {
        $scope: scope,
        $document: $document,
        $parse: $parse
      });
    });
  });

  it('should init default scope.$doc', function() {
    expect(scope.$doc.model).toEqual(item);
    expect(scope.$doc.local.$editable).toEqual({});
  });

  it('should load $doc.local for empty doc', function() {
    scope.$digest();
    expect(scope.$doc.local.title).toBe('');
    expect(scope.$doc.local.description).toBe('');
  });

  it('should load $doc.local copy of doc', function() {
    item.title = 'Title Test!';
    item.description = 'Description Test!';
    scope.$digest();
    expect(scope.$doc.local.title).toBe('Title Test!');
    expect(scope.$doc.local.description).toBe('Description Test!');
  });

  it('should clear $doc.local.$editable after esc pressed', function() {
    var escKeyDown = jQuery.Event('keydown', { keyCode: 27 });
    scope.$doc.local.$editable = { title: true };
    expect($document.bind).toHaveBeenCalledWith('keydown', jasmine.any(Function));
    expect(scope.$doc.local.$editable).not.toEqual({});
    var applies = scope.$apply.calls.length;
    $document.trigger(escKeyDown);
    expect(scope.$apply.calls.length).toBe(applies+1);
    expect(scope.$doc.local.$editable).toEqual({});
    scope.$destroy();
    expect($document.unbind).toHaveBeenCalledWith('keydown', jasmine.any(Function));
  });

  it('should be save', function() {
    expect(scope.$doc.save).toEqual(jasmine.any(Function));
    item.title = 'Title!';
    item.description = 'Description!';
    scope.$digest();
    expect(item.$save).not.toHaveBeenCalled();
    scope.$doc.local.$editable = { title: true };
    scope.$doc.save();
    expect(scope.$doc.local.$editable).toEqual({});
    expect(item.$save).not.toHaveBeenCalled();
    scope.$doc.local.$editable = { title: true };
    scope.$doc.local.title = 'New Title!';
    scope.$doc.save();
    expect(item.$save).toHaveBeenCalled();
    expect(item.title).toBe('New Title!');
    expect(scope.$doc.local.$editable).toEqual({});
  });



});