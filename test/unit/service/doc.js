'use strict';

describe('doc service', function(){

  var doc, $httpBackend, response, appLoading, config;

  beforeEach(function() {
    module('App');

    module(function($provide) {
      appLoading = {
        loading: jasmine.createSpy('appLoading loading'),
        ready: jasmine.createSpy('appLoading ready')
      };
      $provide.value('appLoading', appLoading);

      config = {
        db: {
          name: 'db.name',
          apiKey: 'db.apiKey'
        }
      };
      $provide.value('config', config);

    });

    response = {
      docs: [
        { "_id": { "$oid": "51e5db66e4b0ba1f8763c34f"}, "title": "First document", "description": "description"} ,
        { "_id": { "$oid": "51e5de50e4b054a5b8a1ad17"}, "title": "Second document", "description": "Short description..."}
      ]
    };

    inject(function($injector) {
      doc = $injector.get('doc');

      $httpBackend = $injector.get('$httpBackend');
      $httpBackend
        .whenGET('https://api.mongolab.com/api/1/databases/db.name/collections/docs?apiKey=db.apiKey')
        .respond(response.docs);
      $httpBackend
        .whenPOST('https://api.mongolab.com/api/1/databases/db.name/collections/docs?apiKey=db.apiKey')
        .respond(response.docs[0]);

      $httpBackend
        .whenGET('https://api.mongolab.com/api/1/databases/db.name/collections/docs/51e5db66e4b0ba1f8763c34f?apiKey=db.apiKey')
        .respond(response.docs[0]);
      $httpBackend
        .whenPUT('https://api.mongolab.com/api/1/databases/db.name/collections/docs/51e5db66e4b0ba1f8763c34f?apiKey=db.apiKey')
        .respond(response.docs[0]);
      $httpBackend
        .whenDELETE('https://api.mongolab.com/api/1/databases/db.name/collections/docs/51e5db66e4b0ba1f8763c34f?apiKey=db.apiKey')
        .respond(response.docs[0]);
    });
  });

  afterEach(function() {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  it('should return list of resources', function() {
    var items = doc.list();
    expect(appLoading.loading.calls.length).toBe(1);
    expect(appLoading.ready.calls.length).toBe(0);
    $httpBackend.flush();
    expect(appLoading.loading.calls.length).toBe(1);
    expect(appLoading.ready.calls.length).toBe(1);

    expect(items.length).toBe(2);
    for (var i = 0; i < response.docs.length; i++) {
      expect(items[i].id).toEqual(response.docs[i]._id.$oid);
      expect(items[i].title).toEqual(response.docs[i].title);
      expect(items[i].description).toEqual(response.docs[i].description);
      expect(items[i].$save).toEqual(jasmine.any(Function));
      expect(items[i].$remove).toEqual(jasmine.any(Function));
    }
  });

  it('should insert', function() {
    var done = jasmine.createSpy();
    doc.insert().then(done, done);
    expect(appLoading.loading.calls.length).toBe(1);
    expect(appLoading.ready.calls.length).toBe(0);
    expect(done).not.toHaveBeenCalled();
    $httpBackend.flush();
    expect(done).toHaveBeenCalled();
    expect(appLoading.loading.calls.length).toBe(1);
    expect(appLoading.ready.calls.length).toBe(1);
  });

  it('should get resource by id', function() {
    var item = doc.get(response.docs[0]._id.$oid);
    var done = jasmine.createSpy();
    item.$promise.then(done, done);
    expect(appLoading.loading.calls.length).toBe(1);
    expect(appLoading.ready.calls.length).toBe(0);
    expect(done).not.toHaveBeenCalled();
    $httpBackend.flush();
    expect(done).toHaveBeenCalled();
    expect(appLoading.loading.calls.length).toBe(1);
    expect(appLoading.ready.calls.length).toBe(1);

    expect(item.id).toBe(response.docs[0]._id.$oid);
    expect(item.title).toBe(response.docs[0].title);
    expect(item.description).toBe(response.docs[0].description);

    expect(item.$save).toEqual(jasmine.any(Function));
    expect(item.$remove).toEqual(jasmine.any(Function));
  });

  it('should resource $save', function() {
    var item = doc.get(response.docs[0]._id.$oid);
    $httpBackend.flush();
    item.$save();
    expect(appLoading.loading.calls.length).toBe(2);
    expect(appLoading.ready.calls.length).toBe(1);
    $httpBackend.flush();
    expect(appLoading.loading.calls.length).toBe(2);
    expect(appLoading.ready.calls.length).toBe(2);
  });

  it('should resource $remove', function() {
    var item = doc.get(response.docs[0]._id.$oid);
    $httpBackend.flush();
    item.$remove();
    expect(appLoading.loading.calls.length).toBe(2);
    expect(appLoading.ready.calls.length).toBe(1);
    $httpBackend.flush();
    expect(appLoading.loading.calls.length).toBe(2);
    expect(appLoading.ready.calls.length).toBe(2);
  });

});