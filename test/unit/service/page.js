'use strict';

describe('page service', function(){

  var page;

  beforeEach(function() {
    module('App');
    inject(function($injector) {
      page = $injector.get('page');
    });
  });

  it('should contain an page service', inject(function(page) {
    expect(page).not.toBe(null);
  }));

  it('should be not null title on default', function() {
    expect(page.title).not.toBe(null);
  });

  it('should be changing title', function() {
    page.title = 'Test title!';
    expect(page.title).toBe('Test title!');
  });

});
