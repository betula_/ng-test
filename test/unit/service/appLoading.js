'use strict';

describe('appLoading service', function(){
  var mock = {};

  mock.$RootScope = function() {
    this.$$phase = null;
  };
  mock.$RootScope.prototype.$apply = function() {
    this.beginPhase('$apply');
    this.clearPhase();
  };
  mock.$RootScope.prototype.beginPhase = function(phase) {
    if (this.$$phase) {
      throw new Error('{0} already in progress', this.$$phase);
    }
    this.$$phase = phase;
  };
  mock.$RootScope.prototype.clearPhase = function() {
    this.$$phase = null;
  };


  var rootScope, appLoading;

  beforeEach(function() {
    module('App');

    rootScope = new mock.$RootScope();
    spyOn(rootScope, '$apply');

    module(function($provide) {
      $provide.value('$rootScope', rootScope);
    });

    inject(function($injector) {
      appLoading = $injector.get('appLoading');
    });

  });

  it('should contain an appLoading service', inject(function(appLoading) {
    expect(appLoading).not.toBe(null);
  }));

  it('should be ready on default', function() {
    expect(appLoading.status).toBe('ready');
  });

  it('should be change to loading', function() {
    appLoading.loading();
    expect(appLoading.status).toBe('loading');
    expect(rootScope.$apply).toHaveBeenCalled();
  });

  it('should be change to loading in a $apply phase', function() {
    rootScope.beginPhase('$apply');
    appLoading.loading();
    expect(appLoading.status).toBe('loading');
    expect(rootScope.$apply).not.toHaveBeenCalled();
  });

  it('should be change to loading in a $digest phase', function() {
    rootScope.beginPhase('$digest');
    appLoading.loading();
    expect(appLoading.status).toBe('loading');
    expect(rootScope.$apply).not.toHaveBeenCalled();
  });

  it('should be change to ready from loading status', function() {
    appLoading.loading();
    expect(appLoading.status).toBe('loading');
    appLoading.loading();
    expect(appLoading.status).toBe('loading');
    appLoading.ready();
    expect(appLoading.status).toBe('loading');
    appLoading.ready();
    expect(appLoading.status).toBe('ready');
  });

  it('should be $apply on change ro ready status', function() {
    appLoading.loading();
    expect(rootScope.$apply).toHaveBeenCalled();
    expect(rootScope.$apply.calls.length).toEqual(1);
    appLoading.ready();
    expect(rootScope.$apply.calls.length).toEqual(2);
  });

  it('should be change to ready on a $apply phase', function() {
    rootScope.beginPhase('$apply');
    appLoading.loading();
    expect(appLoading.status).toBe('loading');
    appLoading.ready();
    expect(appLoading.status).toBe('ready');
    expect(rootScope.$apply).not.toHaveBeenCalled();
  });

  it('should be change to ready on a $digest phase', function() {
    appLoading.loading();
    expect(appLoading.status).toBe('loading');
    expect(rootScope.$apply).toHaveBeenCalled();
    rootScope.beginPhase('$digest');
    appLoading.ready();
    expect(appLoading.status).toBe('ready');
    expect(rootScope.$apply.calls.length).toEqual(1);
  });

});
