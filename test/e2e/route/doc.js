'use strict';

describe('doc route', function() {

  it('should otherwise to root path if doc not found', function() {
    browser().navigateTo('#!/unresolved-uid');
    expect(browser().location().path()).toBe('/');
  });

});