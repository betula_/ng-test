'use strict';

describe('home route', function() {

  beforeEach(function() {
    browser().navigateTo('/');
  });

  it('should have root path', function() {
    expect(browser().location().path()).toBe('/');
  });

  it('should otherwise to root path', function() {
    browser().navigateTo('#!/other/undefined/page');
    expect(browser().location().path()).toBe('/');
  });
});