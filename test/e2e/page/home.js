'use strict';

describe('home page', function() {
  beforeEach(function() {
    browser().navigateTo('/');
  });

  it('should have root path', function() {
    expect(browser().location().path()).toBe('/');
  });

  it('should have correct title', function() {
    expect(element('title').text()).toBe('.NG Test');
  });

  it('should have list of items', function() {
    expect(repeater('doc-items li').count()).toBeGreaterThan(0);
  });


});
