module.exports = function(config) {

//  var plugin = require('karma-ng-scenario');
//
//  var files = [
//    'test/e2e/**/*.js'
//  ];
//  plugin['framework:ng-scenario'][1](files);
//  files.shift();
//  files.unshift({
//    pattern: 'var/angular-scenario.js',
//    included: true,
//    served: true,
//    watched: false
//  });
//
//  console.log(files);

  config.set({

    plugins: [
      'karma-ng-scenario',
      'karma-chrome-launcher'
    ],
    frameworks: [
      'ng-scenario'
    ],

    urlRoot: '/__karma/',
    proxies: {
      '/': 'http://localhost:8000/'
    },

    files: [
      'test/e2e/**/*.js'
    ],

    logLevel: config.LOG_INFO,
    logColors: true,

    browsers: ['Chrome'],
    reporters: ['progress']

  });
};
