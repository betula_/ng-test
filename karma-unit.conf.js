
module.exports = function(config) {
  config.set({
    frameworks: ['jasmine'],

    files: [
      'var/jquery.js',

      'var/angular.js',
      'var/angular-route.js',
      'var/angular-cookies.js',
      'var/angular-resource.js',
      'var/angular-mocks.js',

      'src/script/**/*.js',

      'test/unit/**/*.js',

      'var/template/templates.js'
    ],

    logLevel: config.LOG_INFO,
    logColors: true,

    browsers: ['Chrome'],

    reporters: ['progress']

  });
};
