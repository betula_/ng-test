var fs = require('fs');
var path = require('path');
var util = require('util');

module.exports = function(grunt) {

  grunt.initConfig({

    connect: {
      server: {
        options: {
          port: 8000,
          base: 'public'
        }
      }
    },

    shell: {
      bower: {
        command: 'node ./node_modules/bower/bin/bower install',
        options: {
          stdout: false
        }
      }
    },

    copy: {
      angular: {
        files: [{
          expand: true,
          cwd: 'vendor/angular.js/build',
          src: [
            'angular.js',
            'angular-route.js',
            'angular-cookies.js',
            'angular-resource.js',
            'angular-sanitize.js',
            'angular-mobile.js',
            'angular-loader.js',
            'angular-mocks.js',
            'angular-scenario.js'
          ],
          dest: 'var'
        }]
      },
      bower: {
        files: [{
          expand: true,
          cwd: 'bower_components/jquery',
          src: [
            'jquery.js'
          ],
          dest: 'var'
        }]
      }
    },

    less: {
      bootstrap: {
        options: {
          paths: ['vendor/bootstrap/less'],
          variables: 'var/bootstrap-variables.styl',
          base64image: true,
          compress: true
        },
        src: ['src/style/vendor/bootstrap.less'],
        dest: 'var/bootstrap.css'
      }
    },

    stylus: {
      style: {
        options: {
          urlfunc: 'embedurl',
          paths: ['var']
        },
        src: ['src/style/index.styl'],
        dest: 'public/style/style.css'
      }
    },

    concat: {
      vendorjs: {
        src: [
          'var/angular.js',
          'var/angular-route.js',
          'var/angular-cookies.js',
          'var/angular-resource.js'
        ],
        dest: 'public/vendor.js'
      },
      vendorstyle: {
        src: ['var/bootstrap.css'],
        dest: 'public/vendor.css'
      }
    },

    jade: {
      template: {
        files: [
          {
            expand: true,
            cwd: 'src/template',
            src: ['**/*.jade'],
            dest: 'var/template',
            ext: '.html'
          }
        ]
      }
    },

    symlink: {
      angular: {
        dest: 'public/angular.js',
        relativeSrc: '../vendor/angular.js/build',
        options: {
          type: 'dir'
        }
      },
      script: {
        dest: 'public/script',
        relativeSrc: '../src/script',
        options: {
          type: 'dir'
        }
      },
      template: {
        dest: 'public/template',
        relativeSrc: '../var/template',
        options: {
          type: 'dir'
        }
      }
    },

    ngtemplates: {
      app: {
        options: {
          base: 'var/template',
          prepend: '/template/',
          module: {
            name: 'App',
            define: false
          }
        },
        src: 'var/template/**/*.html',
        dest: 'var/template/templates.js'
      }
    },

    karma: {
      unit: {
        configFile: 'karma-unit.conf.js',
        port: 9876,
        runnerPort: 9100
      },
      e2e: {
        configFile: 'karma-e2e.conf.js',
        port: 9877,
        runnerPort: 9101
      }
    },

    watch: {
      style: {
        files: ['src/style/**', '!src/style/vendor/**'],
        tasks: ['stylus:style']
      },
      template: {
        files: ['src/template/**/*.jade'],
        tasks: ['jade:template', 'ngtemplates']
      },
      bootstrap: {
        files: ['src/style/vendor/bootstrap.less'],
        tasks: ['less:bootstrap', 'concat:vendorstyle', 'stylus:style']
      },
      karmaunit: {
        files: ['test/unit/**/*.js'],
        tasks: ['karma:unit:run']
      },
      karame2e: {
        files: ['test/e2e/**/*.js'],
        tasks: ['karma:e2e:run']
      }
    }
  });

  lessTaskRegister(grunt);
  karmaTaskRegister(grunt);

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-jade');
  grunt.loadNpmTasks('grunt-contrib-stylus');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-symlink');
  grunt.loadNpmTasks('grunt-angular-templates');
  grunt.loadNpmTasks('grunt-shell');
  grunt.loadNpmTasks('grunt-contrib-connect');

  grunt.registerTask('build', ['copy:angular','symlink:angular', 'less:bootstrap', 'shell:bower', 'copy:bower']);
  grunt.registerTask('init', ['symlink:script', 'symlink:template']);
  grunt.registerTask('apply', ['concat:vendorjs', 'concat:vendorstyle', 'stylus:style', 'jade:template', 'ngtemplates']);

  grunt.registerTask('default', ['init', 'apply', 'connect:server', 'karma:unit:background', 'karma:e2e:background', 'watch']);
  grunt.registerTask('test', ['init', 'apply', 'connect:server', 'karma']);
};


function lessTaskRegister(grunt) {
  var less = require('less');

  grunt.registerMultiTask('less', 'Build less files', function() {
    var done = this.async();
    var options = this.options({
      paths: [],
      variables: [],
      base64image: false,
      compress: true
    });

    var variables = {};
    grunt.util.async.forEachSeries(this.files, function (f, next) {
      var dest = f.dest;

      var files = f.src.filter(function(filepath) {
        if (!grunt.file.exists(filepath)) {
          grunt.log.warn('Source file "' + filepath + '" not found.');
          return false;
        } else {
          return true;
        }
      });

      if (files.length === 0) {
        if (f.src.length < 1) {
          grunt.log.warn('Destination not written because no source files were found.');
        }
        next();
        return;
      }

      grunt.util.async.concatSeries(files, function(file, _next) {
        compileLess(file, options, function(err, data) {
          if (err) {
            _next(err);
            return;
          }
          var keys = Object.keys(data.variables);
          var i = keys.length;
          while (i--) {
            variables[keys[i]] = data.variables[keys[i]];
          }
          _next(null, data.css);
        });
      }, function(err, styles) {
        if (err) {
          next(err);
          return;
        }
        if (styles.length < 1) {
          grunt.log.warn('Destination not written because compiled files were empty.');
          next();
          return;
        }
        grunt.file.write(dest, styles.join(grunt.util.linefeed));
        grunt.log.writeln('File ' + dest.cyan + ' created.');
        next();
      });

    }, function(err) {
      if (err) {
        done(err);
        return;
      }

      if (options.variables) {
        var i, name, pieces = [];
        for (i in variables) {
          if (variables.hasOwnProperty(i)) {
            name = i;
            name = name.replace(/([a-z0-9])([A-Z])/gm, function(m, a, b) {
              return a+'-'+b;
            }).replace(/([a-z])([0-9])/gmi, function(m, a, b) {
                return a+'-'+b
              }).replace('@','$').toLowerCase();
            pieces.push(name + '=' + variables[i]);
          }
        }
        grunt.file.write(options.variables, pieces.join(grunt.util.linefeed));
        grunt.log.writeln('File ' + options.variables.cyan + ' created.');
      }

      done();
    });

  });

  function compileLess(filename, options, callback) {
    var paths = options.paths || [];
    var text = grunt.file.read(filename);

    var parser = new less.Parser({
      paths: paths,
      filename: filename
    });
    parser.parse(text, function(err, tree) {
      if (err) {
        callback(err);
        return;
      }
      var i, l, j, p;
      var files = [filename].concat(Object.keys(parser.imports.files));
      var dirs = files.map(function(file) {
        return path.dirname(file);
      });
      dirs = dirs.filter(function(_el, _i) {
        return dirs.lastIndexOf(_el) === _i;
      });

      var expression = tree.eval({ frames: [] });

      var _variables = expression.variables();
      var _vars = Object.keys(_variables).map(function (k) {
        return _variables[k];
      });
      var _frames = [new(less.tree.Ruleset)(null, _vars)];
      var _env = {frames: _frames};
      var _value, _name;
      var _values = {};
      var _images = [];
      var _unquoted;

      for (i = 0, l = _vars.length; i < l; i++) {
        _name = _vars[i].name;
        _value = _vars[i].value.eval(_env).toCSS({ compress: true });
        _values[_name] = _value;

        if (typeof _value == 'string') {
          _unquoted = _value;
          if (['"',"'"].indexOf(_value.charAt(0)) != -1) {
            _unquoted = _value.slice(1,-1);
          }
          if (_unquoted.indexOf('.png') >= 0 && options.base64image) {
            for (j = 0; j < dirs.length; j++) {
              p = path.join(dirs[j], _unquoted);
              if (fs.existsSync(p)) {
                _images.push({
                  name: _name,
                  value: _value,
                  path: p
                });
                break;
              }
            }
          }
        }
      }

      var _embed;
      var css = expression.toCSS([], { compress: options.compress });
      for (i = 0, l = _images.length; i < l; i++) {
        _embed = '"data:image/png;base64,' + fs.readFileSync(_images[i].path).toString('base64') + '"';
        css = css.replace(_images[i].value, _embed);
      }

      callback(null, {
        css: css,
        variables: _values
      });
    });
  }
}


function karmaTaskRegister(grunt) {
  var karma = require('karma');
  var spawn = require('child_process').spawn;

  grunt.registerMultiTask('karma', 'Run tests with Karma', function() {
    var configFile = typeof this.data == 'string' ? this.data : this.data.configFile;
    configFile = path.resolve(configFile);
    if (this.flags.background) {
      spawn('node', [
        'node_modules/karma/bin/karma',
        'start',
        configFile,
        '--no-auto-watch',
        this.data.port ? '--port=' + this.data.port : '',
        this.data.runnerPort ? '--runner-port=' + this.data.runnerPort : ''
      ]);
      return;
    }
    var config = { configFile: configFile };
    var data = this.data;
    ['port', 'runnerPort'].forEach(function(element) {
      if (typeof data[element] != 'undefined') {
        config[element] = data[element];
      }
    });
    var done = this.async();
    if (this.flags.run) {
      karma.runner.run(util._extend({ autoWatch: false }, config), done);
      return;
    }
    karma.server.start(util._extend({ singleRun: true }, config), function(code) {
      if(code !== 0) {
        grunt.fail.warn("Test(s) failed");
      }
      done();
    });
  });

}

