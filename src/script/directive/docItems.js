
angular.module('App').directive("docItems", [function () {
  return {
    restrict: 'E',
    scope: {
      items: '='
    },
    templateUrl: '/template/directive/docItems.html'
  };
}]);