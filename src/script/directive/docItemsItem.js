
angular.module('App').directive("docItemsItem", [function () {
  return {
    restrict: 'E',
    scope: {
      item: '='
    },
    replace: true,
    templateUrl: '/template/directive/docItemsItem.html'
  };
}]);