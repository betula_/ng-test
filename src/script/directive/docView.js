
angular.module('App').directive("docView", [function () {
  return {
    restrict: 'E',
    scope: {
      doc: '='
    },
    templateUrl: '/template/directive/docView.html',
    controller: 'DocView'
  };
}]);