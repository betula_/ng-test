
angular.module('App').directive("footer", ['page', function (page) {
  return {
    restrict: 'E',
    templateUrl: '/template/directive/footer.html'
  };
}]);