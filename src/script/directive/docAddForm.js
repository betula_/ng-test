
angular.module('App').directive("docAddForm", [function () {
  return {
    restrict: 'E',
    scope: {
      apply: '&'
    },
    templateUrl: '/template/directive/docAddForm.html'
  };
}]);