
angular.module('App').directive('enterKeyApply', ['$parse', function($parse) {
  return {
    require: '^?form',
    restrict: 'A',
    link: function (scope, element, attrs, form) {
      var action = $parse(attrs.enterKeyApply);
      element.bind('keydown', function(e) {
        if (e.keyCode === 13) {
          if (!form || form.$valid) {
            scope.$apply(function() {
              action(scope);
            });
          }
        }
      });
    }
  }
}]);
