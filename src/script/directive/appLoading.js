
angular.module('App').directive("appLoading", ['appLoading', function (appLoading) {
  return {
    restrict: 'E',
    replace: true,
    scope: {},
    templateUrl: '/template/directive/appLoading.html',
    link: function($scope) {
      $scope.loading = appLoading;
    }
  };
}]);