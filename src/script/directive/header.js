
angular.module('App').directive("header", ['page', 'appLoading', function (page, appLoading) {
  return {
    restrict: 'E',
    templateUrl: '/template/directive/header.html',
    link: function ($scope, element, attrs) {
      $scope.page = page;
    }
  };
}]);