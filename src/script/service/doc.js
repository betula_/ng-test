
angular.module('App').service('doc', ['config', '$resource', 'appLoading', function(config, $resource, appLoading) {

  var loading = function() {
    appLoading.loading();
  };
  var ready = function() {
    appLoading.ready();
  };

  var performDoc = function(item) {
    var i, l;
    if (toString.apply(item) == '[object Array]') {
      for (i = 0, l = item.length; i < l; i++) {
        performDoc(item[i]);
      }
      return;
    }
    if (typeof item._id == 'object') {
      item.id = item._id.$oid;
      delete item._id;
    }
    var list = ['$insert', '$save', '$remove'];
    for (i = 0, l = list.length; i < l; i++) {
      (function(name) {
        var $method = item[name];
        item[name] = function() {
          var promise = $method.apply(this, Array.prototype.slice.call(arguments));
          loading();
          if (name != '$remove') {
            promise.then(function(item) {
              performDoc(item);
              ready();
            }, ready);
          }
          else {
            promise.then(ready, ready);
          }

          return promise;
        };
      })(list[i]);
    }
  };

  function Collection() {
    this.res = $resource('https://api.mongolab.com/api/1/databases/:db/collections/:collection/:id', {
      apiKey: config.db.apiKey,
      db: config.db.name,
      collection: 'docs',
      id: '@id'
    }, {
      get: { method: 'GET' },
      insert: { method: 'POST' },
      save: { method: 'PUT' }
    });
  }

  Collection.prototype.list = function() {
    var items = this.res.query();
    loading();
    items.$promise.then(function(items) {
      ready();
      performDoc(items);
    }, ready);
    return items;
  };

  Collection.prototype.insert = function(data) {
    var item = new this.res(data || {});
    performDoc(item);
    return item.$insert();
  };

  Collection.prototype.get = function(id) {
    var item = this.res.get({ id: id });
    loading();
    item.$promise.then(function(item) {
      ready();
      performDoc(item);
    }, ready);
    return item;
  };

  return new Collection();

}]);