
angular.module('App').service('appLoading', ['$rootScope', function($rootScope) {
  var counter = 0;
  var loading = {
    status: 'ready',
    loading: function() {
      if (counter++ === 0) {
        loading.status = 'loading';
        if (!$rootScope.$$phase) {
          $rootScope.$apply();
        }
      }
    },
    ready: function() {
      if (--counter === 0) {
        loading.status = 'ready';
        if (!$rootScope.$$phase) {
          $rootScope.$apply();
        }
      }
    }
  };
  return loading;

}]);
