
angular.module('App').config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/', {
    templateUrl: '/template/page/home.html'
  });
  $routeProvider.otherwise({
    redirectTo: '/'
  });
}]);

