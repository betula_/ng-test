
angular.module('App').config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/:id', {
    templateUrl: '/template/page/doc.html',
    resolve: {
      doc: ['doc', '$route', '$location', function(doc, $route, $location) {
        var promise = doc.get($route.current.params.id).$promise;
        promise.then(null, function() {
          $location.url('/');
        });
        return promise;
      }]
    },
    controller: 'PageDoc'
  });
}]);

