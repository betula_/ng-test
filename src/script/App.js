'use strict';

angular.module('App', ['ngRoute', 'ngCookies', 'ngResource']);

angular.module('App').config(['$locationProvider', function($locationProvider) {
  $locationProvider.hashPrefix('!');
}]);

angular.module('App').value('config', {
  db: {
    name: 'a09711',
    apiKey: 'u8c6kpH3Ql9fBlr87ueVhhVheGMak82w'
  }
});


