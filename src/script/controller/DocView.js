
angular.module('App').controller('DocView', ['$scope', '$document', '$parse', function($scope, $document, $parse) {

  $scope.$doc = {
    model: $scope.doc,
    local: {
      $editable: {}
    }
  };

  $scope.$doc.model.$promise.then(function(item) {
    var local = $scope.$doc.local;
    local.title = item.title || '';
    local.description = item.description || '';
  });

  var documentKeyDown = function(e) {
    if(e.keyCode === 27) {
      $scope.$apply(function() {
        $parse('$doc.local.$editable').assign($scope, {});
      });
    }
  };

  $document.bind('keydown', documentKeyDown);
  $scope.$on('$destroy', function() {
    $document.unbind('keydown', documentKeyDown);
  });

  $scope.$doc.save = function() {
    var i;
    var model = this.model;
    var local = this.local;
    var change = false;

    for (i in local) {
      if (local.hasOwnProperty(i) && i != '$editable') {
        if (local[i] != model[i]) {
          model[i] = local[i];
          change = true;
        }
      }
    }
    if (change) {
      model.$save();
    }
    local.$editable = {};
  };


}]);