
angular.module('App').controller('DocList', ['$scope', 'doc', function($scope, doc) {
  $scope.items = [];

  $scope.loadItems = function() {
    var items = doc.list();
    items.$promise.then(function() {
      var i, l;
      for (i = 0, l = items.length; i < l; i++) {
        (function(item) {
          var $remove = item.$remove;
          item.$remove = function() {
            var promise = $remove.call(this);
            promise.then(function() {
              $scope.loadItems();
            });
            return promise;
          }
        })(items[i]);
      }
      $scope.items = items;
    });
  };

  $scope.addItem = function(title, description) {

    var promise = doc.insert({
      title: title,
      description: description
    });
    promise.then(function() {
      $scope.loadItems();
    });
  };

  $scope.loadItems();
}]);